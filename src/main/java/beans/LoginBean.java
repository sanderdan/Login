package beans;

import entity.User;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import service.CrudService;
import utils.PasswordService;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.ejb.EJBException;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by Sander on 2015-12-09.
 */
@ManagedBean
@Named("loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private static final long serialVersionUID = 7765876811740798583L;

    @Inject
    CrudService dao;

    @PostConstruct
    public void init() {
        this.navigationBean = new NavigationBean();
    }

    private boolean loggedIn;
    private String username;
    private String password;
    private String role;


    @ManagedProperty(value = "#{navigationBean}")
    private NavigationBean navigationBean;

    public String doRegister() {
        User user = new User();
        user.setUsername(Jsoup.clean(username, Whitelist.simpleText()));
        user.setRole(role);
        try {
            user.setPassword(PasswordService.createHash(password));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        dao.create(user);

        FacesMessage msg = new FacesMessage(username + " created!", "You can now login");
        msg.setDetail(FacesMessage.FACES_MESSAGES);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return navigationBean.toLogin();
    }


    /**
     * Login operation.
     *
     * @return
     */
    public String doLogin() throws InvalidKeySpecException, NoSuchAlgorithmException {
        User userToCheck;

        // First check if user with that username exists
        try {
            userToCheck = dao.findByNamedQuery(User.findByUsername, username);
        } catch (EJBException e) {
            e.printStackTrace();
            FacesMessage msg = new FacesMessage("Login error!", "ERROR MSG");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);

            return navigationBean.toLogin();
        }

        // If so, check passwords
        if (PasswordService.validatePassword(password, userToCheck.getPassword())) {

            // Successful login
            loggedIn = true;

//             Check if it's an admin
            if (userToCheck.getRole().equals("admin")) {
                return navigationBean.redirectToAdmin();
            } else if (userToCheck.getRole().equals("user")) {
                return navigationBean.toWelcome();
            }

        }

        // else if passwords didn't match
        FacesMessage msg = new FacesMessage("Login error!", "ERROR MSG");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().

                addMessage(null, msg);

        // To to login page
        return navigationBean.toLogin();

    }

    /**
     * Logout operation.
     *
     * @return
     */
    public String doLogout() {

        // Set the paremeter indicating that user is logged in to false
        loggedIn = false;

        // Set logout message
        FacesMessage msg = new FacesMessage("Logout success!", "INFO MSG");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);

        return navigationBean.toLogin();
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
