package service;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Sander on 2015-11-15.
 */
@Stateless
@Local(CrudService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CrudServiceBean implements CrudService, Serializable {

    @PersistenceContext
    public EntityManager em;

    @Override
    public <T> T create(T t) {
        this.em.persist(t);
        return t;
    }

    /**
     * Find one
     * @param type
     * @param id
     * @param <T>
     * @return
     */
    @Override
    public <T> T find(Class<T> type, Object id) {
        return this.em.find(type, id);
    }

    /**
     * Delete
     * @param type
     * @param o
     */
    @Override
    public void delete(Class type, Object o) {
        try {
            Object id = this.em.getReference(type, o);
            this.em.remove(id);
        } catch (EntityNotFoundException e) {
            // we want to remove it...
        }
    }

    /**
     * Update
     * @param t
     * @param <T>
     * @return
     */
    @Override
    public <T> T update(T t) {
        return (T) this.em.merge(t);
    }

    /**
     * Find all for one Entity
     * @param namedQueryName
     * @return
     */
    @Override
    public List<Object> findByNamedQuery(String namedQueryName) {
        return this.em.createNamedQuery(namedQueryName).getResultList();
    }

    @Override
    public <T> T findByNamedQuery(String namedQueryName, String username) {
        return (T) this.em.createNamedQuery(namedQueryName)
                .setParameter("Username", username)
                .getSingleResult();
    }


    @Override
    public List<Object> findByNamedQueryWithId(String namedQueryName, long id) {

        return this.em.createNamedQuery(namedQueryName)
                .setParameter("Id", id).getResultList();
    }
}