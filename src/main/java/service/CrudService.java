package service;

import java.util.List;

/**
 * Created by Sander on 2015-11-15.
 */
public interface CrudService {

    <T> T create(T t);

    <T> T find(Class<T> type, Object id);

    <T> T update(T t);

    void delete(Class type, Object t);

    List findByNamedQuery(String queryName);

    <T> T findByNamedQuery(String queryName, String string);

    List findByNamedQueryWithId(String queryName, long id);


}
