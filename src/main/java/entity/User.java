package entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Created by Sander on 2015-12-09.
 */
@Entity
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = User.findAll, query = "SELECT u FROM User u"),
        @NamedQuery(name = User.findById, query = "SELECT u From User u WHERE u.id =:Id"),
        @NamedQuery(name = User.findByUsername, query = "SELECT u From User u WHERE u.username =:Username")
})
public class User implements Serializable {

    static final String PREFIX = "entity.User.";
    public static final String findAll = PREFIX + "findAll";
    public static final String findById = PREFIX + "findById";
    public static final String findByUsername = PREFIX + "findByUsername";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String username;
    private String password;
    private String role;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
